<?php
    require_once "database/QueryBuilder.php";
    class Categoria extends QueryBuilder
    {
        private $id;
        private $nombre;
        private $numimagen;
        private $descripcion;

        public function __construct($id = 0, $nombre ="",  $numimagen = 0, $descripcion ="")
        {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->numimagen= $numimagen;
            $this->descripcion = $descripcion;
        }
        
        public function getId() {return $this->id;}
        public function getNombre() { return $this->nombre; }
        public function getnumimagen() { return $this->numimagen; }
        public function getDescripcion() { return $this->descripcion; }

        public function toArray(): array
    {
        return 
        [

            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),

            "numimagen"=>$this->getNumimagen(),

            "descripcion"=>$this->getDescripcion()
        ];
    }
    }
?>