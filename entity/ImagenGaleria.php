<?php
require_once "database/IEntity.php";
class ImagenGaleria implements IEntity
{
    const RUTA_IMAGENES_PORTAFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALLERY = "images/index/gallery/";
    private $id;
    private $nombre;
    private $descripcion;
    private $numVisualizaciones;
    private $numLikes;
    private $numDownloads;
    private $fk_categoria;

    public function __construct( $id = 0, string $nombre ="", string $descripcion ="", $categoria = 0, $numVisualizaciones=0, $numLikes=0, $numDownloads=0)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->fk_categoria = $categoria;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
    }

    public function getId() {return $this->id;}
    public function getNombre() { return $this->nombre; }
    public function getDescripcion() { return $this->descripcion; }
    public function getCategoria()   {return $this->fk_categoria;}
    public function getNumVisualizaciones(){ return $this->numVisualizaciones; }
    public function getNumLikes(){ return $this->numLikes; }
    public function getNumDownloads(){ return $this->numDownloads; }

    public function getURLPortfolio() : string
    {
        return  self::RUTA_IMAGENES_PORTAFOLIO."$this->nombre";
    }

    public function getURLGallery() 
    {
        return self::RUTA_IMAGENES_GALLERY."$this->nombre";
    }
    
    public function toArray(): array
    {
        return 
        [

            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),

            "descripcion"=>$this->getDescripcion(),

            "fk_categoria"=>$this->getCategoria(),

            "numVisualizaciones"=>$this->getNumVisualizaciones(),

            "numLikes"=>$this->getNumLikes(),

            "numDownloads"=>$this->getNumDownloads()

        ];
    }
}
?>