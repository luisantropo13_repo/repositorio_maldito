<?php
include "utils/utils.php";
require "entity/ImagenGaleria.php";
require_once "repository/ImagenGaleriaRepository.php";

$config = require_once("app/config.php");
App::bind("config", $config);
$imagenGaleriaRepository = new ImagenGaleriaRepository(); 
$array_galeria = $imagenGaleriaRepository->findAll();

require_once "views/index.view.php";
