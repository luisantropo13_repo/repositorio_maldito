<?php
    require_once "database/Connection.php";
    require_once "exceptions/AppException.php";
    class App{
        //Almacena los datos en nuestro contenedor
        private static $container = [];

        public static function bind (string $key, $value)
        {
            static::$container[$key] = $value;
        }

        public static function get(string $key)
        {
            if(!array_key_exists($key, static::$container))          
                throw new AppException("No se ha encontrado la clave $key en el contenedor.");
            return static::$container[$key];
        }

        public static function getConfigDB ( ) {
            return static::$container["config"];
        }

        public static function getConnection()
        {
            if(!array_key_exists("connection", static::$container))
                static::$container["connection"] = connection::make( App::getConfigDB() );
            return static::$container["connection"];
        }
    }
?>