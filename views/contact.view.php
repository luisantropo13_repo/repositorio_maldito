<?php

	const ERROR_NOMBRE  = 0; 
	const ERROR_CORREO  = 1;
	const ERROR_SUBJECT = 2;
	const ERROR_MESSAGE = 3;

	const ERROR_TEXT_NAME    = "Nombre no válido.";
	const ERROR_TEXT_CORREO  = "Email no válido";
	const ERROR_TEXT_SUBJECT = "Subject no válido.";
	const ERROR_TEXT_MESSAGE = "Mensaje no válido";

	require("funct.views.php");

	$name = isset($_POST["name"]) ? $_POST["name"] : null;
	$email = isset($_POST["correo"]) ? $_POST["correo"] : null;
	$subject = isset($_POST["sub"]) ? $_POST["sub"] : null;
	$mensa = isset($_POST["mensage"]) ? $_POST["mensage"] : null;

	$errores = array();
	if ($_SERVER["REQUEST_METHOD"] == "POST"){

		if(!validaNombre($name))
		array_push($errores, array(
			"codeError"=> ERROR_NOMBRE,
			"text"     => ERROR_TEXT_NAME
		));

		if(!validaEmail($email))
		array_push($errores, array(
			"codeError"=> ERROR_CORREO,
			"text"     => ERROR_TEXT_CORREO
		));

		if(!validaSubject($subject))
		array_push($errores, array(
			"codeError"=> ERROR_SUBJECT,
			"text"     => ERROR_TEXT_SUBJECT
		));

		if(!validaMensage($mensa))
		array_push($errores, array(
			"codeError"=> ERROR_MESSAGE,
			"text"     => ERROR_TEXT_MESSAGE
		));
	}
	
	function printError( $type )
	{
		global $errores;

		foreach( $errores as $error)
		{
			if( $error["codeError"] == $type)
			{
				echo $error["text"];
				break;
			}	
		}
	}
?>


<?php include __DIR__ . "/partials/inicio-doc.part.php"; ?>

<?php include __DIR__ . "/partials/nav.part.php"; ?> 


<!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACT US</h1>
       	   <hr>
       	   <p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
		   

			<form class="form-horizontal" method="post">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">First Name</label>
					<input class="form-control" type="text" name="name" required>
					<?php printError( ERROR_NOMBRE ); ?>
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Last Name</label>
	       	  		<input class="form-control" type="text">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Email</label>
					<input class="form-control" type="text" name="correo" required>
					<?php printError( ERROR_CORREO ); ?>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Subject</label>
					<input class="form-control" type="text" name="sub" required>
					<?php printError( ERROR_SUBJECT ); ?>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Message</label>
	       	  		<textarea class="form-control" name="mensage"></textarea>
					<button class="pull-right btn btn-lg sr-button">SEND</button>
					<?php printError( ERROR_MESSAGE ); ?>
	       	  	</div>
	       	  </div>
		   </form>
		   
		   <hr class="divider">
		   <?php print_r($_POST); ?>
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<!-- Jquery -->
   <script type="text/javascript" src="js/jquery.min.js"></script>
   <!-- Bootstrap core Javascript -->
   <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins -->
   <script type="text/javascript" src="js/jquery.easing.min.js"></script>
   <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
   <script type="text/javascript" src="js/scrollreveal.min.js"></script>
   <script type="text/javascript" src="js/script.js"></script>
</body>
</html>