<?php 
    function validaNombre($value){
        return !( empty(trim($value)) || $value == null );
    }

    function validaEmail($value){
        return filter_var($value, FILTER_VALIDATE_EMAIL );
    }

    function validaSubject($value){
        return !(trim($value) == "");
    }

    function validaMensage($value){  // Opcional.
        return true;
    }
?>  