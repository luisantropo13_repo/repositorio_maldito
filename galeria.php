<?php
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "database/QueryBuilder.php";
require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";
require_once "core/App.php";
require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";



$errorres [] = ""; //ESTA AQUI
$imagenes = array();

try
{
    $config = require_once("app/config.php");

    App::bind("config", $config);

    $imagenGaleriaRepository = new ImagenGaleriaRepository(); 
    $categoriaRepository = new CategoriaRepository(); 

    if ($_SERVER["REQUEST_METHOD"]==="POST") 
    {
        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);       

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);

        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY,ImagenGaleria::RUTA_IMAGENES_PORTAFOLIO);

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFiles(), $descripcion, $categoria);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Se ha guardado la imagen en la BBDD";

    }
    $imagenes = $imagenGaleriaRepository->findAll(); 
    $categorias = $categoriaRepository->findAll();
}
catch (FileException $fileException) 
{

    $errores [] = $fileException->getMessage();

}
catch (QueryException $queryException) 
{

    $errores [] = $queryException->getMessage();

}     
catch (AppException $appException)
{
    $errores [] = $appException->getMessage();
}


require_once "views/galeria.view.php";

?>